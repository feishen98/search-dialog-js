# search_dialogJS

自带数据过滤的搜索框，对不同单个字符搜索且关键字变色

## Show
<img src="art/search_dialog.gif" height="60%" width="60%"/>

## Usage
将pages下index文件夹，拷贝到自己项目中并修改文件名字，然后通过import导入js即可使用了
1，给js中的originalList按照对应的数据结构赋值对应数据
2，根据对应的需要，按照如下赋值：
```
 //普通列表搜索框
    showDialog1(e) {
        this.isShowIcon = false
        this.loadingStyle ='loading'
        this.isShowLoading = false
        this.initData()
        this.$element('dialog').show()
    }
```
```
//Logo列表搜索框
    showDialog2(e) {
        this.isShowIcon = true;
        this.loadingStyle ='loading'
        this.isShowLoading = false
        this.initData()
        this.$element('dialog').show()
    }
```
```
 //网络加载搜索框
    showDialog3(e) {
        this.isShowIcon = true;
        this.isShowLoading = true
        this.initData()
        this.$element('dialog').show()
    }
//网络数据需要特别处理，就是在
 //数据处理
    onDataHandle() { 
        ……
          if (this.isShowLoading) {
           this.loadingStyle ='loadingShow' //请求前
          //这里可以做网络请求，拿到数据后
          that.loadingStyle ='loading'  //请求完成后
          //先给this.originalList = … 赋值，然后在调用一次初始化数据调用这个方法：this.initData()，
          //最后再调用  that.filterData()方法后，就完成了过滤并显示出列表了了
     }
  }    
```
由此便可完成使用，详情请看index.js的code

## 问题
1，我在模拟器上测试一切正常，但是在测试手机（2.0.0 Developer Beta3）版本上，在search组件搜索框在输入字符后，右边会出现一个“x”清除按钮，
在需要清除所有字符的时候，在模拟器上点击后，search组件onchange事件是正常响应的，但是在测试手机上，onchange事件没有出现任何响应，导致数据无法还原到初始化状态，列表状态没法还原。
2，在输入框字符后，在模拟器上能正常过滤，且style也正常，但是在测试手机上，过滤数据出现混乱，过滤数据偶现错误。
多次试验发现是list列表刷新的bug，显示错误后，点击item拿到的是正确的数据，但是不是正确的显示。由此推断。
3,对于重叠词输入，只会匹配到最先靠前的字符会变色，后面的字符不会变色。
4，被输入法覆盖几次后，list列表就显示不全了。