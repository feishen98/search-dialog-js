
//原始数据模型
export class OriginalList{

    id = 0;
    isShow = true;
    imgUrl = '';
    content='';

    constructor(id,isShow,imgUrl,content){
        this.id = id;
        this.isShow = isShow;
        this.imgUrl = imgUrl;
        this.content = content;
    }
}

//初始化后的数据模型
export class ChangeList{
    id = 0;
    isShow = true;
    imgUrl = '';
    content='';
    list = new Array();

    constructor(id,isShow,imgUrl,list){
        this.id = id;
        this.isShow = isShow;
        this.imgUrl = imgUrl;
        this.list = list;
    }
}

export class ChangeStrList{
    isRed= '';
    str='';

    constructor(isRed,str){
        this.isRed = isRed;
        this.str = str;
    }
}